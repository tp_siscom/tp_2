.code16
    mov $msg, %si 
    mov $0x0e, %ah
loop:
    lodsb
    or %al, %al
    jz halt
    int $0x10
    jmp loop
halt:
    hlt
msg:
    .asciz "\nSu computador ha sido hackeado por el grupo 5.\n\rNo toque nada, ya tenemos su clave de homebanking..."
